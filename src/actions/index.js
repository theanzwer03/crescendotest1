import API from '../apis'
export const selectRecipe = recipe => {
  return {
    type: "SELECT_RECIPE",
    payload: recipe
  }
}

export const getRecipes = ()=> async (dispatch) => {
  const response = await API.get('/recipes');
  return dispatch({type: "GET_RECIPES", payload: response})
}

export const getSpecials = ()=> async (dispatch) => {
  const response = await API.get('/specials');
  return dispatch({type: "GET_SPECIALS", payload: response})
}

export const addRecipe = (values) => (dispatch) => {
  API.post('/recipes', {
    ...values
  })
  .then(function (response) {
    console.log("RESPONSE", response);
    return dispatch({type: "ADD_RECIPE", payload: response})
  })
  .catch(function (error) {
    console.log(error);
  });
}