import React, {useEffect} from 'react';
import {connect, useDispatch} from 'react-redux';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import RecipeList from './RecipeList';
import RecipeView from './RecipeView';
import {getRecipes} from '../actions';


export const App = props => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getRecipes());
  }, [dispatch])

  if(!props.recipes)return <div>Loading...</div>
  return (
    <Router>
      <Switch>
        <Route path="/:uuid" component={RecipeView} />
        <Route exact path="/">
          <RecipeList />
        </Route>
      </Switch>
    </Router>
  )
}
const mapStateToProps = (state, ownProps) => {
  return {
    recipes: state.recipes
  }
}
export default connect(mapStateToProps)(App);