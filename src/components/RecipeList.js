import React from 'react';
import RecipeListView from './RecipeListView';
import {connect} from 'react-redux';

export const RecipeList = ({recipes}) => {
  return (
    <div className="container">
      <div className="card-container">
        <h2>Recipes</h2>
        <div className="recipe-list">
          {recipes.map((recipe)=>{
            return (
              <RecipeListView key={recipe.uuid} recipe={recipe} />
            )
          })}
        </div>
      </div>
      
    </div>
  )
}
const mapStateToProps = (state, ownProps) => {
  return {
    recipes: state.recipes
  }
}
export default connect(mapStateToProps)(RecipeList);