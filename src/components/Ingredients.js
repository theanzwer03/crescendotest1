import React, {useEffect, useState} from 'react';

const Ingredients = ({ingredient, specials}) => {
  const [specialIngredient, setSpecialIngredient] = useState('');
  useEffect(()=>{
    let special = specials.find(special=>special.ingredientId === ingredient.uuid);
    setSpecialIngredient(special);
  }, [specials, ingredient]);
  return (
    <li>{ingredient.amount} {ingredient.measurement} {ingredient.name} 
      {
        specialIngredient ? 
        <ul className="special">
          <li>{specialIngredient.type}</li>
          <li>{specialIngredient.title}</li>
          <li>{specialIngredient.text}</li>
        </ul>
        : null
      }
    </li>
  )
}

export default Ingredients;