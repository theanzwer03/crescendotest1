import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {selectRecipe} from '../actions';

export const RecipeListView = props => {
  return( 
    <Link
      to={`/${props.recipe.uuid}`}
      onClick={()=>{props.selectRecipe(props.recipe)}} >
      <div className="card">
        <div className="img-holder">
          <img alt={props.recipe.title} src={`${process.env.REACT_APP_BASE_URL}${props.recipe.images.small}`} />
        </div>
        <div className="content">
          <div className="title">{props.recipe.title}</div>
          <div className="description">{props.recipe.description}</div>
        </div>
      </div>
    </Link>
  )
}

const mapStateToProps = state => {
  return {
    selectedRecipe: state.selectedRecipe,
  }
}
export default connect(mapStateToProps, {selectRecipe})(RecipeListView);