import React from 'react';
import sprite from '../img/sprite.svg';
/*
  props: icon,
*/
const SVG = ({icon, onClick, className}) => {
  return (
    <svg className={className} onClick={()=> onClick ? onClick() : null}>
      <use xlinkHref={`${sprite}#${icon}`}></use>
    </svg>
  )
}

export default SVG;