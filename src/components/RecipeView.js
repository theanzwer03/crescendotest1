import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect, useDispatch} from 'react-redux';
import Ingredients from './Ingredients';
import {getSpecials} from '../actions';
import SVG from './SVG';

export const RecipeView = ({selectedRecipe,specials}) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getSpecials());
  }, [dispatch]);

  if(!specials){
    return <div>Loading...</div>;
  }
  return (
    <div className="recipe-page container">
      <Link to="/" className="close-btn">
        <SVG  icon="icon-close" />
      </Link>
      <div className="recipe-container">
        <div>
          <div className="img-holder">
            <img alt={selectedRecipe.images.title} src={`${process.env.REACT_APP_BASE_URL}${selectedRecipe.images.medium}`} />
          </div>
        </div>
        <div className="recipe-content">
          <h2>{selectedRecipe.title}</h2>
          <ul className="extra">
            <li>{selectedRecipe.servings} servings</li>
            <li>{selectedRecipe.prepTime} prep time</li>
            <li>{selectedRecipe.servings} cook time</li>
          </ul>
          <p>{selectedRecipe.description}</p>
          
          <div className="list-container">
            <div>
              <h4>INGREDIENTS</h4>
              <ul className="ingredients">
                {selectedRecipe.ingredients.map((ingredient)=>{
                  return <Ingredients key={ingredient.uuid} ingredient={ingredient} specials={specials}/>
                })}
              </ul>
            </div>
            <div>
              <h4>DIRECTIONS</h4>
              <ul className="directions">
                {selectedRecipe.directions.map((direction, index)=>{
                  return (
                    <li key={`direction-${index}`}>{direction.instructions} {direction.optional ? <span className="optional">OPTIONAL</span> : null}</li>
                  )
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>

      
    </div>
  )
}
const mapStateToProps = (state, ownProps) => {
  let id = ownProps.match.params.uuid;
  return {
    recipes: state.recipes,
    selectedRecipe: state.recipes.find(recipe=>recipe.uuid === id),
    specials: state.specials,
    cart: state.cart
  }
}

export default connect(mapStateToProps)(RecipeView);