import {combineReducers} from 'redux';

const recipeReducer = (recipes = null, actions) => {
  switch (actions.type) {
    case "GET_RECIPES": {
      return actions.payload.data;
    }
    case "ADD_RECIPE": {
      return actions.payload.data;
    }
    default: {
      return recipes
    }
  }
}
const specialsReducer = (specials = null, actions) => {
  switch (actions.type) {
    case "GET_SPECIALS": {
      return actions.payload.data;
    }
    default: {
      return specials
    }
  }
}

const selectedRecipeReducer = (selectedRecipe = null, actions) => {
  switch(actions.type){
    case "SELECT_RECIPE": {
      return actions.payload;
    }
    default: {
      return selectedRecipe;
    }
  } 
}

export default combineReducers({
  recipes: recipeReducer,
  selectedRecipe: selectedRecipeReducer,
  specials: specialsReducer,
})